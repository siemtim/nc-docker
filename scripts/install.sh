#!/ bin/bash

# EXIT on Error & Print executed commands
set -xe

apt-get update
apt-get -y install \
    curl \
    libxml2 \
    openssl \
    sudo

# Move PHP inis
mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini";

# Install PHP Extensions
echo "Download php extension script"
curl -fsSLo /usr/local/bin/install-php-extensions https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions 
chmod uga+x /usr/local/bin/install-php-extensions

echo "Starting Installation of PHP Extension Installation"
install-php-extensions gd apcu bcmath bz2 zip pdo_mysql intl ldap imap gmp redis imagick oauth pcntl exif

# Interpretation der Github-Seite https://github.com/mlocati/docker-php-extension-installer hat ergeben, dass diese beiden Schritte nicht mehr nötig sind.
# docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp
# docker-php-ext-install gd apcu /var/lib/apt/lists/bcmath bz2 zip pdo_mysql intl ldap imap gmp redis imagick oauth pcntl exif
echo "Installation der Pakete per apt und install-php-extensions beendet"

#Entfernen des PHP Extension Scriptes
rm /usr/local/bin/install-php-extensions
echo "/usr/local/bin/install-php-extensions entfernt"

# Apache Configuration
echo "Starting Apache Configuration"
a2enmod rewrite
a2enmod headers
a2enmod env
a2enmod dir
a2enmod mime
a2dissite 000-default
a2ensite  nextcloud
echo "Konfiguration der Apache-Module und Deativierung der default-site abgeschlossen."

echo "Deleting unused Apache Configuration"
for CFILE in $(ls /etc/apache2/sites-available/)
do
    if ! [[ "${CFILE}" == "nextcloud.conf" ]]
    then
        echo "Deleting /etc/apache2/sites-available/${CFILE}"
        rm "/etc/apache2/sites-available/${CFILE}"
    fi
done

# Adjust nextcloud dir
mkdir -p /var/nc-data/
mkdir -p /var/www/html/config/
chown -R root:www-data /var/nc-data/
chown -R root:www-data /var/www/html/config/
chmod -R 770 /var/nc-data/
chmod -R 770 /var/www/html/config/


echo "Deleting unused packages and apt cache"
apt-get -y remove \
    curl
apt-get -y autoremove
apt-get clean
rm -Rf /var/lib/apt/lists/
