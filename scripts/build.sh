#!/bin/bash

#EXIT on Error
set -e

echo "Unpacking App archives"
for APP_FILE in $(ls /download/apps)
do
    echo "Unpacking App file ${APP_FILE}"
    tar -xzf "/download/apps/${APP_FILE}" -C nextcloud/apps/
done

echo "Removing Unwanted Apps"
cat remove_apps.txt | grep -v '^$' | while read LINE
do
    if ! [[ "${LINE}" == "" ]]
    then
        rm -R "nextcloud/apps/${LINE}"
    fi
done

if ! [ -f "nextcloud/.htaccess" ]
then
    echo ".htaccess is missing in build, security flaw"
    exit 1
fi

openssl rand -hex 16 > nextcloud/VERSION

