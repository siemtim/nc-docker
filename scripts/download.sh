#!/bin/bash

#EXIT on Error
set -e

ENABLE_FILE='./ENABLE_APPS'

echo "Downloading Nextcloud"

NC_URL=$(cat ./nextcloud_url.txt | sed 's/ *$//g')
NC_URL_HASH=$(echo ${NC_URL} | openssl dgst -md5 -binary | xxd -p)

if ! [[ -f "/download/${NC_URL_HASH}" ]]
then
  curl -fsSLo nextcloud.tar.bz2 "${NC_URL}"
else 
  cp "/download/${NC_URL_HASH}" nextcloud.tar.bz2 
fi

echo "Downloading apps"

touch "${ENABLE_FILE}"

cat ./app_urls.txt | grep -v '^$' | while read LINE
do
  URL=$(echo "$LINE" | awk '{print $1}')
  NAME=$(echo "$LINE" | awk '{print $2}')
  STATUS=$(echo "$LINE" | awk '{print $3}')
  URL_HASH=$(echo ${URL} | openssl dgst -md5 -binary | xxd -p)

  if ! [[ -f "/download/${URL_HASH}" ]]
  then
    echo "Download App '$NAME' ('$URL')"
    curl -fsSLo "./apps/${NAME}.tar.gz" "$URL"
  else
    echo "Copy '$NAME' from Cache"
    cp "/download/${URL_HASH}" "./apps/${NAME}.tar.gz"
  fi

  if [[ "{$STATUS}" == 'enabled' ]]
  then
    echo "Adding ${NAME} to the Enabled-File (${ENABLE_FILE})"
    echo "${NAME}" | tee -a "${ENABLE_FILE}" > /dev/null
  fi
done
