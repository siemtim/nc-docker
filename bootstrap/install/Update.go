package install

import (
	"os/exec"

	"tim-siemers.de/nextcloud-bootstrap/config"
	"tim-siemers.de/nextcloud-bootstrap/log"
)

type UpdateConfiguration struct {
	config *config.Config
}

func NewUpdateConfiguration(config *config.Config) *UpdateConfiguration {
	return &UpdateConfiguration{config}
}

func (config *UpdateConfiguration) Update() error {
	cmd := exec.Command("sudo", "-u", "www-data", "php", "occ", "upgrade")
	err := cmd.Run()
	if err != nil {
		log.Error("Could not update nextcloud: %s\n", err)
		return err
	} else {
		return nil
	}
}
