package install

import (
	"bufio"
	"math/rand"
	"os"
	"os/exec"
	"strings"
	"time"

	"tim-siemers.de/nextcloud-bootstrap/config"
	"tim-siemers.de/nextcloud-bootstrap/log"
)

const PASSWORD_CHARACTER_SET = "QWERTZUIPLKJHGFDSAYXCVBNM23456789"
const PASSWORD_LENGTH = 16
const ENABLE_APPS_FILE = ".ENABLE_APPS"

type InstallationConfig struct {
	config *config.Config
}

func NewInstallationConfig(config *config.Config) *InstallationConfig {
	return &InstallationConfig{config}
}

func (config *InstallationConfig) Install() error {
	log.Info("Creating File config/CAN_INSTALL")
	f, fErr := os.Create("config/CAN_INSTALL")
	if fErr != nil {
		log.Error("Could not create config/CAN_INSTALL")
		return fErr
	} else {
		f.Close()
	}

	log.Info("Creating & starting installation command")
	cmdParts := config.InstalllationCommand()
	cmd := exec.Command(cmdParts[0], cmdParts[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Error("Could not Install Nextcloud:", err)
		return err
	}

	log.Info("Enabling Apps from .ENABLE_APPS file")
	appErr := config.EnableApps()
	if appErr != nil {
		log.Error("Could not Enable Apps, Error is not fatal, continue", appErr)
	}

	cronErr := config.SetCronType()
	if cronErr != nil {
		return cronErr
	}
	return nil
}

func (config *InstallationConfig) InstalllationCommand() []string {
	if config.config.NcAdminPw == "" {
		config.config.NcAdminPw = randomAdminPassword()
		log.Printf("Admin Cresentials: Username: 'admin', Password: '%s'\n", config.config.NcAdminPw)
	}

	return []string{
		"sudo", "-u", "www-data",
		"php", "occ", "maintenance:install",
		"--database", "mysql",
		"--database-host", config.config.DbHost,
		"--database-name", config.config.DbName,
		"--database-user", config.config.DbUser,
		"--database-pass", config.config.DbPassword,
		"--data-dir", "/var/nc-data/",
		"--admin-user", "admin",
		"--admin-pass", config.config.NcAdminPw,
		"--no-interaction",
	}
}

func (config *InstallationConfig) EnableApps() error {
	file, fErr := os.Open(ENABLE_APPS_FILE)
	if fErr != nil {
		log.Error("Cannot Open enable Apps File", fErr)
		return fErr
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.Trim(line, "\r\t\n ")
		if len(line) > 1 {
			err := config.EnableApp(line)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (config *InstallationConfig) EnableApp(appName string) error {
	log.Info("Enabling App", appName)
	cmd := exec.Command("sudo", "-u", "www-data", "php", "occ", "app:enable", appName)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Error("Error enabling App", appName, err)
		return err
	}
	return nil
}

func (config *InstallationConfig) SetCronType() error {
	log.Info("Setting Cron Type to cron")
	cmd := exec.Command("sudo", "-u", "www-data", "php", "occ", "background:cron")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Error("Cannot set Cron type to corn", err)
		return err
	}
	return nil
}

func randomAdminPassword() string {
	rand.Seed(time.Now().Unix())
	var output strings.Builder

	for i := 0; i < PASSWORD_LENGTH; i++ {
		random := rand.Intn(len(PASSWORD_CHARACTER_SET))
		randomChar := PASSWORD_CHARACTER_SET[random]
		output.WriteString(string(randomChar))
	}
	return output.String()

}
