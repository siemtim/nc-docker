package install

import (
	"context"
	"io/ioutil"
	"strings"

	"tim-siemers.de/nextcloud-bootstrap/config"
	"tim-siemers.de/nextcloud-bootstrap/log"
	"tim-siemers.de/nextcloud-bootstrap/redis"
	"tim-siemers.de/nextcloud-bootstrap/db"
	"tim-siemers.de/nextcloud-bootstrap/fsutil"
)

const (
	VERSION_FILE   = "./VERSION"
	NC_CONFIG_FILE = "./config/config.php"
)

type InstallationService struct {
	install *InstallationConfig
	update  *UpdateConfiguration
	config  *config.Config

	configurationService redis.ConfigurationService
	installationService  redis.InstallationService
}

func NewInstallationService(ctx context.Context, config *config.Config) (*InstallationService, error) {
	install := NewInstallationConfig(config)
	update := NewUpdateConfiguration(config)

	log.Info("Waiting for DB to start")
	dbErr := db.AwaitDb(ctx, config)
	if dbErr != nil {
		log.Error("No Connection to DB Could be established")
		return nil, dbErr
	}

	rc, rcErr := redis.RedisClient(ctx, config)
	if rcErr != nil {
		return nil, rcErr
	}
	rl, rlErr := redis.RedisLockClient(ctx, config)
	if rlErr != nil {
		return nil, rlErr
	}

	is := redis.NewInstallationService(rl, rc)
	cs := redis.NewConfigurationService(rc)

	return &InstallationService{install, update, config, cs, is}, nil

}

func (is *InstallationService) InstallOrUpdate(ctx context.Context) error {
	//Read own version from file
	versionF, vfErr := readVersionFile()
	if vfErr != nil {
		return vfErr
	}
	log.Info("Read own version from file:", versionF)

	//lock configuration servcie
	if lErr := is.installationService.Lock(ctx); lErr != nil {
		return lErr
	}
	defer is.installationService.UnLock(ctx)
	log.Info("Locked Installation")

	//Compare Versions
	version := is.installationService.CompareVersions(ctx, versionF)

	if version == redis.NC_VERSION_EQUAL {
		log.Info("Install / Update: No action needed; Pull Config")
		pullErr := is.pullConfig(ctx)
		if pullErr != nil {
			return pullErr
		}
	} else if version == redis.NC_VERSION_DIFFERENT {
		log.Info("Performing Update")
		is.update.Update()
		is.postInstallUpdate(ctx, versionF)

	} else {
		log.Info("Performing Install")
		is.install.Install()
		is.postInstallUpdate(ctx, versionF)
	}

	tErr := InitDataDir()
	if tErr != nil {
		log.Error("Cannot init data dir")
		return tErr
	}

	return nil
}

func (is *InstallationService) pullConfig(ctx context.Context) error {
	log.Info("Try to pull Configuration")
	if configphp, ok := is.configurationService.Get(ctx); ok {
		log.Info("Got Configuration from Redis")
		log.Debug("Config:\n", configphp)
		err := ioutil.WriteFile(NC_CONFIG_FILE, []byte(configphp), 0770)
		if err != nil {
			log.Error("Cannot write config.php:", err)
			return err
		}

		if pErr := fsutil.SetFilePermissionsAndOwner(NC_CONFIG_FILE, "root", "www-data", 0770); pErr != nil {
			log.Errorf("Permissions and owner of %s not set", NC_CONFIG_FILE)
			return pErr
		}
		
		log.Info("Config Written to file", NC_CONFIG_FILE)
	} else {
		log.Warn("Could not get configuration from redis, this may be an error")
	}
	return nil
}

func (is *InstallationService) postInstallUpdate(ctx context.Context, version string) {
	is.installationService.SetVersion(ctx, version)

	conf, err := readConfigFile()

	if err != nil {
		log.Panic("Unexpected error while reading config/config.php:", err)
	}

	is.configurationService.Put(ctx, conf)
}

func readConfigFile() (string, error) {
	b, err := ioutil.ReadFile(NC_CONFIG_FILE)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func readVersionFile() (string, error) {
	b, err := ioutil.ReadFile(VERSION_FILE)
	if err != nil {
		return "", err
	}
	return strings.Trim(string(b), " \t\n\r"), nil
}
