package install

import (
    "os"
    "path"

    "tim-siemers.de/nextcloud-bootstrap/log"
)

const NC_DATA_DIR = "/var/nc-data"

func InitDataDir() error {
    filename := path.Join(NC_DATA_DIR, ".ocdata")
    log.Info("Touch file in Nextcloud Data Dir", filename)
    
    f, err := os.Create(filename)
    if err != nil {
        log.Error("Cannot Touch file to init data dir", filename, err)
        return err
    }

    return f.Close()
}

