package confd

import (
	"io"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"tim-siemers.de/nextcloud-bootstrap/log"
	"tim-siemers.de/nextcloud-bootstrap/fsutil"
)

const CONF_D_DIR = "/conf.d"
const NC_CONF_DIR = "/var/www/html/config"
const FILE_SUFFIX = ".config.php"

func CopyConfd() error {
	names, lErr := listConfd()
	if lErr != nil {
		log.Error("Error while listing conf.d files:", lErr)
		return lErr
	}

	for _, name := range names {
		cErr := copyConfd(name)
		if cErr != nil {
			log.Error("Error while copying conf.d file:", cErr)
			return cErr
		}
	}
	return nil
}

func listConfd() ([]string, error) {
	paths := make([]string, 0, 30)

	files, err := ioutil.ReadDir(CONF_D_DIR)
	if err != nil {
		return paths, err
	}

	for _, f := range files {
		if !f.IsDir() && strings.HasSuffix(f.Name(), FILE_SUFFIX) {
			paths = append(paths, f.Name())
		}
	}
	return paths, nil
}

func copyConfd(src string) error {
	log.Infof("Copy conf.d file '%s'", src)
	from := path.Join(CONF_D_DIR,  src)
	to   := path.Join(NC_CONF_DIR, src)
	
	if cErr := copy(from, to); cErr != nil {
		log.Errorf("Cannot Copy conf.d %s -> %s: %s", from, to, cErr)
		return cErr
	}

	if pErr := fsutil.SetFilePermissionsAndOwner(to, "root", "www-data", 0750); pErr != nil {
		log.Error("Could not Set Permission and Owner of conf.d file", to, ":", pErr)
		return pErr
	}
	return nil
}

func copy(src, dst string) error {
	srcR, srcErr := os.Open(src)
	if srcErr != nil {
		return srcErr
	}
	defer srcR.Close()

	dstW, dstErr := os.Create(dst)
	if dstErr != nil {
		return dstErr
	}
	defer dstW.Close()

	_, cErr := io.Copy(dstW, srcR)
	return cErr
}
