package log

var logger Logger = DefaultLogger()

func Debug(a ...interface{}) {
	logger.Debug(a...)
}
func Debugf(format string, a ...interface{}) {
	logger.Debugf(format, a...)
}

func Info(a ...interface{}) {
	logger.Info(a...)
}
func Infof(format string, a ...interface{}) {
	logger.Infof(format, a...)
}

func Warn(a ...interface{}) {
	logger.Warn(a...)
}

func Warnf(format string, a ...interface{}) {
	logger.Warnf(format, a...)
}

func Error(a ...interface{}) {
	logger.Error(a...)
}

func Errorf(format string, a ...interface{}) {
	logger.Errorf(format, a...)
}

func Panic(a ...interface{}) {
	logger.Panic(a...)
}
func Panicf(format string, a ...interface{}) {
	logger.Panicf(format, a...)
}

func Println(a ...interface{}) {
	logger.Println(a...)
}

func Printf(format string, a ...interface{}) {
	logger.Printf(format, a...)
}

func SetDefaultLogger(l Logger) {
	if l == nil {
		logger.Panic("There must be a default logger, nil given")
	}
	logger = l
}

func SetLogLevel(lvl int) {
	if lvl < LvlDebug || lvl > LvlError {
		Warnf("SetLogLevel: Log level out of range; range = [%d, %d]; lvl = %d", LvlDebug, LvlError, lvl)
	}

	logger.(*loggerImpl).level = lvl
}
