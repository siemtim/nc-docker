package log

type Logger interface {
	Debug(...interface{})
	Debugf(format string, a ...interface{})

	Info(...interface{})
	Infof(format string, a ...interface{})

	Warn(...interface{})
	Warnf(format string, a ...interface{})

	Error(...interface{})
	Errorf(format string, a ...interface{})

	Panic(...interface{})
	Panicf(format string, a ...interface{})

	Println(a ...interface{})
	Printf(format string, a ...interface{})
}
