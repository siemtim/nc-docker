package log

import (
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	LvlDebug = 0
	LvlInfo
	LvlWarn
	LvlError
)

type loggerImpl struct {
	logger *log.Logger
	level  int
}

var _ Logger = &loggerImpl{}

func NewLogger(logger *log.Logger, lvl int) Logger {
	if logger == nil {
		panic("There Must be a log.Logger, nil given")
	}

	if lvl < LvlDebug || lvl > LvlError {
		log.Printf("WARN SetLogLevel: Log level out of range; range = [%d, %d]; lvl = %d", LvlDebug, LvlError, lvl)
	}

	return &loggerImpl{logger, lvl}
}

func DefaultLogger() Logger {
	lvl := LvlInfo
	if env, envPresent := os.LookupEnv("GO_LOGLEVEL"); envPresent {
		switch env {
		case "DEBUG":
			lvl = LvlDebug
		case "WARN":
			lvl = LvlWarn
		case "ERROR":
			lvl = LvlError
		}
	}

	logger := NewLogger(log.Default(), lvl)
	logger.Info("New logger with loglevel from Env or default:", lvl)
	return logger
}

func (l *loggerImpl) Debug(a ...interface{}) {
	l.log("DEBUG", LvlDebug, a...)
}

func (l *loggerImpl) Debugf(format string, a ...interface{}) {
	l.logf("DEBUG", LvlDebug, format, a...)
}

func (l *loggerImpl) Info(a ...interface{}) {
	l.log("INFO", LvlInfo, a...)
}

func (l *loggerImpl) Infof(format string, a ...interface{}) {
	l.logf("INFO", LvlInfo, format, a...)
}

func (l *loggerImpl) Warn(a ...interface{}) {
	l.log("WARN", LvlWarn, a...)
}

func (l *loggerImpl) Warnf(format string, a ...interface{}) {
	l.logf("WARN", LvlWarn, format, a...)
}

func (l *loggerImpl) Error(a ...interface{}) {
	l.log("ERROR", LvlError, a...)
}

func (l *loggerImpl) Errorf(format string, a ...interface{}) {
	l.logf("ERROR", LvlError, format, a...)
}

func (l *loggerImpl) Panic(a ...interface{}) {
	l.log("ERROR -> PANIC", LvlError, a...)
	panic(fmt.Sprintln(a...))
}

func (l *loggerImpl) Panicf(format string, a ...interface{}) {
	l.logf("ERROR -> PANIC", LvlError, format, a...)
	panic(fmt.Sprintf(format, a...))
}

func (l *loggerImpl) Println(a ...interface{}) {
	l.logger.Println(a...)
}

func (l *loggerImpl) Printf(format string, a ...interface{}) {
	l.logger.Printf(format, a...)
}

func (l *loggerImpl) log(prefix string, lvl int, a ...interface{}) {
	if l.level <= lvl {
		proc := os.Args[0]
		as := append([]interface{}{prefix, proc}, a...)
		l.logger.Println(as...)
	}
}

func (l *loggerImpl) logf(prefix string, lvl int, format string, a ...interface{}) {
	if l.level <= lvl {
		s := fmt.Sprintf(format, a...)
		s = strings.Trim(s, " \n")
		proc := os.Args[0]
		l.logger.Println(prefix, proc, s)
	}
}
