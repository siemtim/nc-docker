package cron

type Cron interface {
	Start() error
	Stop()
	Wait() <-chan int
}
