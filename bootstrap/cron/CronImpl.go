package cron

import (
	"errors"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"tim-siemers.de/nextcloud-bootstrap/log"
)

type cronImpl struct {
	mutex       sync.Mutex
	active      bool
	cronRunning bool

	stopped chan int

	ticker *time.Ticker
}

var _ Cron = &cronImpl{}

func NewCron() Cron {
	return &cronImpl{
		active:      false,
		cronRunning: false,
		stopped:     make(chan int, 10),
		ticker:      nil,
	}
}

func (c *cronImpl) Start() error {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	if !c.active {
		c.ticker = time.NewTicker(time.Minute * 5)
		go c.listenTicker()
		return nil
	} else {
		log.Error("Cannot Start Cron, already running")
		return errors.New("ALREADY RUNNING")
	}
}

func (c *cronImpl) isActive() bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	return c.active
}

func (c *cronImpl) listenTicker() {
	for c.isActive() {
		select {
		case <-c.ticker.C:
			c.run()
		case <-c.stopped:
			c.mutex.Lock()
			c.active = false
			c.ticker.Stop()
			c.mutex.Unlock()
		}
	}
}

func (c *cronImpl) Stop() {
	c.stopped <- 0
}

func (c *cronImpl) run() {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	if c.cronRunning {
		log.Warn("Not Running cron: Another cron is active")
	} else {
		log.Info("Running Corn Job")
		c.cronRunning = true
		cmd := c.cmd()
		if err := cmd.Start(); err != nil {
			log.Error("Cannot start cron, stopping cron: ", err)
			c.Stop()
		} else {
			go c.awaitCmd(cmd)
		}

	}
}

func (c *cronImpl) awaitCmd(cmd *exec.Cmd) {

	if wErr := cmd.Wait(); wErr != nil {
		log.Error("Error executing Corn, stopping cron:", wErr)
		c.Stop()
	} else {
		log.Info("Cron Job finished")
		c.mutex.Lock()
		defer c.mutex.Unlock()
		c.cronRunning = false
	}
}

func (c *cronImpl) cmd() *exec.Cmd {
	command := []string{
		"sudo", "-u", "www-data",
		"php", "-f", "/var/www/html/cron.php",
	}

	log.Debugf("Nextcloud Cron Command: '%s'", strings.Join(command, " "))

	cron := exec.Command(command[0], command[1:]...)
	cron.Stderr = os.Stderr
	cron.Stdout = os.Stdout

	return cron
}

func (c *cronImpl) Wait() <-chan int {
	ch := make(chan int)

	go func() {
		running := true
		for running {
			c.mutex.Lock()
			running = c.cronRunning || c.active
			c.mutex.Unlock()

			if running {
				time.Sleep(time.Second * 2)
			}
		}
		ch <- 0
	}()
	return ch
}
