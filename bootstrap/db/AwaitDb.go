package db

import (
    "context"
    "time"
    
    "tim-siemers.de/nextcloud-bootstrap/config"
    "tim-siemers.de/nextcloud-bootstrap/log"
)

//TODO Implement properly

func AwaitDb(ctx context.Context, conf *config.Config) error {
    log.Warn("Waiting for DB not properly implemented, sleeping instead for 20 s")
    timer := time.NewTimer(time.Second * 20)
    defer timer.Stop()

    select {
        case <- timer.C:
            log.Info("Waited for DB")
        case <- ctx.Done():
            log.Warn("Context Done, did not wait for DB")
    }

    return nil
}
