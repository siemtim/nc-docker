package fsutil

import (
    "os"
    "os/user"
    "strconv"

    "tim-siemers.de/nextcloud-bootstrap/log"
)

func SetFileOwner(file, uid, gid string) error {
    u, uErr := user.Lookup(uid)
	if uErr != nil {
		log.Errorf("Could not lookup user %s: %s", uid, uErr)
		return uErr
	}

    uidInt, uidErr := strconv.ParseInt(u.Uid, 0, 0)
    if uidErr != nil {
        log.Error("Cannot parse int uid:", uidErr)
        return uidErr
    }

	group, gErr := user.LookupGroup(gid)
	if gErr != nil {
		log.Errorf("Could not lookup group %s: %s", group, gErr)
		return gErr
	}

    gidInt, gidErr := strconv.ParseInt(group.Gid, 0, 0)
    if gidErr != nil {
        log.Error("Cannot parse int gid:", gidErr)
        return gidErr
    }

    if ownErr := os.Chown(file, int(uidInt), int(gidInt)); ownErr != nil {
		log.Errorf("Could not Chown %s: %s", file, ownErr)
		return ownErr
	}

    return nil
}

func SetFilePermission(file string, mode os.FileMode) error {
    if permErr := os.Chmod(file, mode); permErr != nil {
		log.Errorf("Could not Change Permission of %s to %d: %s", file, mode, permErr)
		return permErr
	}
	return nil
}

func SetFilePermissionsAndOwner(file, uid, gid string, mode os.FileMode) error {
    if oErr := SetFileOwner(file, uid, gid); oErr != nil {
        return oErr
    }
    if pErr := SetFilePermission(file, mode); pErr != nil {
        return pErr
    }
    return nil
}
