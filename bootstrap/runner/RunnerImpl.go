package runner

import (
	"errors"
	"os"
	"os/exec"
	"strings"
	"time"
	"sync"

	"tim-siemers.de/nextcloud-bootstrap/log"
)

type runnerImpl struct {
	commandString []string
	cmd           *exec.Cmd
	wg            sync.WaitGroup
}

var _ Runner = &runnerImpl{}

func NewRunnerFromArgv() Runner {
	return &runnerImpl{
		commandString: os.Args[1:], 
		cmd: nil,
	}
}

func NewRunner(cmd ...string) Runner {
	return &runnerImpl{
		commandString: cmd, 
		cmd: nil,
	}
}

func (r *runnerImpl) Start() error {
	if r.cmd != nil {
		log.Error("Cannot start Cmd >>>", strings.Join(r.commandString, " "), "<<< Already Running")
		return errors.New("ALREADY RUNNING")
	}

	r.cmd = exec.Command(r.commandString[0], r.commandString[1:]...)
	r.cmd.Stdout = os.Stdout
	r.cmd.Stderr = os.Stderr

	err := r.cmd.Start()
	if err != nil {
		log.Error("Error starting Runner >>>", strings.Join(r.commandString, " "), "<<<", err)
		return err
	}
	r.wg.Add(1)
	go func(){
		wErr := r.cmd.Wait()
		if wErr != nil {
			log.Error("Error while waiting for runner:", wErr)
			r.wg.Done()
		}
	}()
	return nil
}

func (r *runnerImpl) Stop() {
	if r.cmd == nil {
		log.Error("Cannot stop runner, not started")
	} else {
		log.Info("Signaling runner process to terminate")
		sErr := r.cmd.Process.Signal(os.Interrupt)
		if sErr != nil {
			log.Error("Cannot Signal Process, Killing")
			r.cmd.Process.Kill()
		} else {
			go r.backupKill()
		}
	}
}

func (r *runnerImpl) backupKill() {
	log.Debug("Backup for killing: Waiting for 3 Seconds")
	timer := time.NewTimer(time.Second * 3)
	chWait := r.Wait()
	select {
	case <-chWait:
	case <-timer.C:
		log.Error("Process not terminated within 3 Seconds, Killing")
		r.cmd.Process.Kill()
	}
}

func (r *runnerImpl) Wait() <-chan int {
	ch := make(chan int)
	go func() {
		r.wg.Wait()
		ch <- 0
	}()
	return ch
}
