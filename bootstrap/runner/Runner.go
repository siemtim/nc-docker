package runner

type Runner interface {
	Start() error
	Stop()
	Wait() <-chan int
}
