package config_test

import (
	"os"
	"testing"

	"tim-siemers.de/nextcloud-bootstrap/config"
)

func TestImport(t *testing.T) {
	os.Setenv("MYSQL_HOST", "mysql.local")
	os.Setenv("MYSQL_USER", "user")
	os.Setenv("MYSQL_PASSWORD", "password")
	os.Setenv("MYSQL_DATABASE", "db")

	config, err := config.ParseConfigFromEnv()
	if err != nil {
		t.Error("unexpected error", err)
		t.Fail()
	}

	AssertEqual(t, config.DbHost, "mysql.local")
	AssertEqual(t, config.DbPassword, "password")
	AssertEqual(t, config.DbUser, "user")
	AssertEqual(t, config.DbName, "db")

	if config.DbPort != 3306 {
		t.Error("mysql port should be default 3306 but is", config.DbPort)
		t.Fail()
	}
}

func AssertEqual(t *testing.T, a, b string) {
	if a != b {
		t.Errorf("Expected '%s' but got '%s'\n", b, a)
		t.Fail()
	}
}
