package config

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"strconv"
)

type Config struct {
	DbHost        string `env:"MYSQL_HOST"`
	DbPort        int    `env:"MYSQL_PORT" default:"3306" required:"false"`
	DbUser        string `env:"MYSQL_USER"`
	DbPassword    string `env:"MYSQL_PASSWORD"`
	DbName        string `env:"MYSQL_DATABASE"`
	NcAdminPw     string `env:"NEXTCLOUD_ADMIN_PASSWORD" default:"" required:"false"`

	RedisHost     string `env:"REDIS_HOST"`
	RedisPort     int    `env:"REDIS_PORT" default:"6379" required:"false"`
	RedisPassword string `env:"REDIS_PASSWORD" default:"" required:"false"`
}

type configSetter struct {
	Kind         reflect.Kind
	FieldIndex   int
	EnvName      string
	Required     bool
	DefaultValue string
}

func (config *Config) getConfigSetter(out chan *configSetter) {
	sType := reflect.TypeOf(*config)

	for i := 0; i < sType.NumField(); i++ {
		stField := sType.Field(i)
		setter := &configSetter{stField.Type.Kind(), i, "", true, ""}

		if envName, envOk := stField.Tag.Lookup("env"); envOk {
			setter.EnvName = envName
		}

		if reqStr, reqOk := stField.Tag.Lookup("required"); reqOk {
			req, parseErr := strconv.ParseBool(reqStr)
			panicif(parseErr)
			setter.Required = req
		}

		if defaultValue, defOk := stField.Tag.Lookup("default"); defOk {
			setter.DefaultValue = defaultValue
		} else if !setter.Required {
			panic("Not equired Field in Config must have a default value.")
		}

		if setter.EnvName != "" {
			out <- setter
		}
	}

	close(out)
}

func (config *Config) readEnv(setter *configSetter) error {
	field := reflect.ValueOf(config).Elem().Field(setter.FieldIndex)
	if envValue, envOk := os.LookupEnv(setter.EnvName); envOk {
		return config.setValue(&field, envValue)
	} else if setter.Required {
		return errors.New("REQUIRED ENV NOT PRESENT:" + setter.EnvName)
	} else {
		return config.setValue(&field, setter.DefaultValue)
	}
}

func (config *Config) setValue(field *reflect.Value, value string) error {
	kind := field.Type().Kind()
	if kind == reflect.String {
		field.SetString(value)
	} else if kind == reflect.Int {
		intValue, pErr := strconv.ParseInt(value, 10, 0)
		if pErr != nil {
			return pErr
		}
		field.SetInt(intValue)
	} else {
		return errors.New(fmt.Sprint("UNKNOWN REFLECTION KIND:", kind))
	}
	return nil
}

func ParseConfigFromEnv() (*Config, error) {
	config := &Config{}
	chSetter := make(chan *configSetter)

	go config.getConfigSetter(chSetter)

	for setter := range chSetter {
		err := config.readEnv(setter)
		if err != nil {
			return nil, err
		}
	}

	return config, nil
}

func panicif(err error) {
	if err != nil {
		panic(err.Error())
	}
}
