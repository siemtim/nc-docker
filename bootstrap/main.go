package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"tim-siemers.de/nextcloud-bootstrap/confd"
	"tim-siemers.de/nextcloud-bootstrap/config"
	"tim-siemers.de/nextcloud-bootstrap/cron"
	"tim-siemers.de/nextcloud-bootstrap/install"
	"tim-siemers.de/nextcloud-bootstrap/log"
	"tim-siemers.de/nextcloud-bootstrap/runner"
)

func main() {
	var err error

	ctx := context.Background()

	// Copy Conf.d
	log.Info("Copy files from /conf.d -> /var/www/html/config")
	err = confd.CopyConfd()
	if err != nil {
		log.Error("Cannot copy conf.d", err)
		panic(err)
	}

	log.Info("Parsinf configuration from env")
	config, confErr := config.ParseConfigFromEnv()
	if confErr != nil {
		log.Error("Cannot parse config from env:", confErr)
		panic(confErr)
	}

	log.Info("Starting installation / update")
	installService, iErr := install.NewInstallationService(ctx, config)
	ictx, icancel := context.WithDeadline(ctx, time.Now().Add(time.Minute*5))
	if iErr != nil {
		log.Error("Cannot init InstallationsService:", iErr)
		panic(iErr)
	}
	installService.InstallOrUpdate(ictx)
	icancel()

	log.Info("Starting main process")
	mainRunner := runner.NewRunnerFromArgv()
	mainErr := mainRunner.Start()
	if mainErr != nil {
		log.Panic("Cannot start main Runner", mainErr)
	}
	mainCh := mainRunner.Wait()

	log.Info("Starting Cron")
	cron := cron.NewCron()
	if cronErr := cron.Start(); cronErr != nil {
		log.Panic("Cannot Start Cron", cronErr)
	}
	cronCh := cron.Wait()

	log.Info("Creating Signal Listener")
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	select {
	case sig := <-sigs:
		log.Info("Received Signal", sig, "-> Terminating")
		cron.Stop()
		mainRunner.Stop()
		log.Info("Waiting for main Runner to finish")
		<-mainCh
		log.Info("Main Runner finished")
	case <-mainRunner.Wait():
		log.Warn("Unexpected exit of main Runner -> Terminating")
		cron.Stop()
	}

	log.Info("Waiting for Cron to finish")

	<-cronCh
	log.Info("Cron finished")
}
