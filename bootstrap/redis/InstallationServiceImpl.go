package redis

import (
	"context"
	"errors"
	"time"

	"github.com/bsm/redislock"
	"github.com/go-redis/redis/v8"

	"tim-siemers.de/nextcloud-bootstrap/log"
)

const (
	REDIS_VERSION_KEY      = "bootstrap/version"
	REDIS_INSTALL_LOCK_KEY = "bootstrap/install-lock"
	R_CHARS                = "qwertzuiopasdfghjklyxcvbnm1234567890QWERTZUIOPASDFGHJKLYXCVBNM"
)

type installationServiceImpl struct {
	lockClient  *redislock.Client
	redisClient *redis.Client
	lock        *redislock.Lock
}

var _ InstallationService = &installationServiceImpl{}

func NewInstallationService(lClient *redislock.Client, rClient *redis.Client) InstallationService {
	return &installationServiceImpl{lClient, rClient, nil}
}

func (install *installationServiceImpl) Lock(ctx context.Context) error {
	if install.lock != nil {
		return errors.New("COULD NOAQUIRE LOCK: LOCK IS PRESENT")
	}

	backoff := redislock.LinearBackoff(500 * time.Millisecond)
	cctx, cancel := context.WithDeadline(ctx, time.Now().Add(time.Minute))
	defer cancel()

	log.Info("Try to obtain lock for Install / Update")
	l, err := install.lockClient.Obtain(cctx, REDIS_INSTALL_LOCK_KEY, time.Minute*10, &redislock.Options{
		RetryStrategy: backoff,
	})

	if err == redislock.ErrNotObtained {
		log.Warn("Could not obtain Lock for Install / Update:", err)
		return err
	} else if err != nil {
		log.Error("Unexpected Error while obtaining Lock for Intsall / Update:", err)
		return err
	} else {
		log.Info("Obtained Lock for install, 10 Minutes TTL")
		install.lock = l
		return nil
	}
}

func (install *installationServiceImpl) CompareVersions(ctx context.Context, ownVersion string) int {
	res := install.redisClient.Get(ctx, REDIS_VERSION_KEY)
	if res.Err() == redis.Nil {
		return NC_VERSION_NONE
	} else if res.Err() != nil {
		log.Error("Error comparing versions", res.Err())
		panic("Error comparing versions")
	} else {
		v, _ := res.Result()
		log.Infof("Comparing installed versions own = %s, installed = %s", ownVersion, v)
		if v == ownVersion {
			return NC_VERSION_EQUAL
		} else {
			return NC_VERSION_DIFFERENT
		}
	}
}

func (install *installationServiceImpl) SetVersion(ctx context.Context, version string) {
	log.Info("Setting installed version to", version)
	err := install.redisClient.Set(ctx, REDIS_VERSION_KEY, version, 0).Err()
	if err != nil {
		log.Error("Unexpected error setting installed version to", version, ":", err)
	}
}

func (install *installationServiceImpl) UnLock(ctx context.Context) {
	if install.lock == nil {
		panic("NIL POINTER: NO LOCK PRESENT")
	}

	err := install.lock.Release(ctx)
	install.lock = nil

	if err != nil {
		log.Warn("Unexpected error while releasing lock for Install / Update: ", err)
	}
}
