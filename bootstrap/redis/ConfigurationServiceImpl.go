package redis

import (
	"context"

	redis "github.com/go-redis/redis/v8"
	"tim-siemers.de/nextcloud-bootstrap/log"
)

const (
	REDIS_CONFIG_KEY        = "bootstrap/config"
	REDIS_CONFIG_PUBSUB_KEY = "bootstrap/config-pubsub"
)

type configurationServiceImpl struct {
	client *redis.Client
}

var _ ConfigurationService = &configurationServiceImpl{}

func NewConfigurationService(client *redis.Client) ConfigurationService {
	return &configurationServiceImpl{client}
}

func (c *configurationServiceImpl) Subscribe(ctx context.Context, config_ch chan string) CancelHook {
	pubsub := c.client.Subscribe(ctx, REDIS_CONFIG_PUBSUB_KEY)

	cancel := func() {
		pubsub.Close()
	}

	ch := pubsub.Channel()

	go func() {
		for msg := range ch {
			config_ch <- msg.Payload
		}
	}()
	return cancel
}

func (c *configurationServiceImpl) Put(ctx context.Context, config string) {
	err := c.client.Set(ctx, REDIS_CONFIG_KEY, config, 0).Err()
	if err != nil {
		log.Error("Cannot set config")
	}

	pErr := c.client.Publish(ctx, REDIS_CONFIG_PUBSUB_KEY, config).Err()
	if pErr != nil {
		log.Error("Cannot publish config change")
	}

}

func (c *configurationServiceImpl) Get(ctx context.Context) (string, bool) {
	res := c.client.Get(ctx, REDIS_CONFIG_KEY)

	if res.Err() == redis.Nil {
		return "", false
	} else if res.Err() != nil {
		log.Error("Error getting configuration from redis", res.Err())
		return "", false
	} else {
		resStr, rErr := res.Result()
		if rErr != nil {
			log.Error("Unexpected Error:", rErr)
			return "", false
		}
		return resStr, true
	}
}
