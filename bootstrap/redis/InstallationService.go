package redis

import (
    "context"
)

const (
    NC_VERSION_EQUAL     = 0
    NC_VERSION_DIFFERENT = 1
    NC_VERSION_NONE      = -1
)

/*
    Redis Keys:
    - bootstrap/version                 -> current installed version
    - bootstrap/install-lock            -> key to lock for installation / update
*/

type InstallationService interface {
    //Blocks until the Installation Lock can be aquired
    Lock(ctx context.Context) error
    
    // Compares the ownVersion to the Version in the redis store
    // return:
    //    0 -> no difference
    //    1 -> version differs, UPDATE
    //   -1 -> no version present INSTALL
    CompareVersions(ctx context.Context, ownVersion string) int

    //Sets the version
    SetVersion(ctx context.Context, version string)

    //Releases the aquired lock
    // Panci if no lock is aquired
    UnLock(ctx context.Context)
}

