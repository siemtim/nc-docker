package redis

import (
	"context"
	"fmt"
	"time"

	"github.com/bsm/redislock"
	"github.com/go-redis/redis/v8"

	"tim-siemers.de/nextcloud-bootstrap/config"
)

var redisClient *redis.Client = nil

func RedisClient(ctx context.Context, conf *config.Config) (*redis.Client, error) {

	cctx, cancel := context.WithDeadline(ctx, time.Now().Add(time.Second*3))
	defer cancel()

	if redisClient == nil {
		addr := fmt.Sprintf("%s:%d", conf.RedisHost, conf.RedisPort)

		c := redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: conf.RedisPassword,
			DB:       0,
		})

		if err := c.Ping(cctx).Err(); err != nil {
			return nil, err
		}
		redisClient = c
	}

	return redisClient, nil
}

var redisLockClient *redislock.Client = nil

func RedisLockClient(ctx context.Context, conf *config.Config) (*redislock.Client, error) {

	if redisLockClient == nil {
		client, err := RedisClient(ctx, conf)
		if err != nil {
			return nil, err
		}
		redisLockClient = redislock.New(client)
	}

	return redisLockClient, nil
}
