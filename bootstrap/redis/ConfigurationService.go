package redis

import (
    "context"
)

type CancelHook func()

type ConfigurationService interface {
    Subscribe(ctx context.Context, config_ch chan string) CancelHook

    Put(ctx context.Context, config string)
    
    Get(ctx context.Context) (string, bool)
}
