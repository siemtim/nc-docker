<?php
    $CONFIG = [
        'appstoreenabled' => false,
        'apps_paths' => [
            [
                'path'=> '/var/www/html/apps',
                'url' => '/apps',
                'writable' => false,
            ],
        ],
    ];
?>
