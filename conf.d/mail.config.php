<?php
    $CONFIG = (function(){
        if (getenv('SMTP_HOST')) {
            return [
                "mail_smtpmode"     => "smtp",
                "mail_smtphost"     => getenv("SMTP_HOST"),
                "mail_smtpport"     => intval(getenv('SMTP_PORT')),
                'mail_smtpsecure'   => (getenv('SMTP_SECURE') ? getenv('SMTP_SECURE') : ''),
                "mail_smtpauth"     => boolval(getenv('SMTP_AUTH')),
                "mail_smtpname"     => getenv("SMTP_USER"),
                "mail_smtppassword" => getenv("SMTP_PASSWORD"),
                "mail_smtpauthtype" => getenv("SMTP_AUTH_TYPE") ? getenv("SMTP_AUTH_TYPE") : 'LOGIN',
                "mail_domain"       => getenv("MAIL_DOMAIN"),
                'mail_from_address' => getenv('MAIL_FROM_ADDRESS')
            ];
        }
        else return [];
    })();
?>
