<?php
    $COMFIG = (function(){
        $c = [];

        if (getenv('NEXTCLOUD_PROXY')) {
            $c['proxy'] = getenv('NEXTCLOUD_PROXY');
            $c['proxyexclude'] = explode(',', getenv('NO_PROXY'));
        }

        $c['trusted_proxies'] =  explode(',', getenv('TRUSTED_PROXY'));
        return $c;
    })();
?>
