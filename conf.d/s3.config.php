<?php
    $CONFIG = (function(){
        if(getenv('S3_BUCKET')) {
            return [
                'objectstore' => [
                    'class' => '\\OC\\Files\\ObjectStore\\S3',
                    'arguments' => [
                        'bucket' => getenv('S3_BUCKET'),
                        'autocreate' => boolval(getenv('S3_BUCKET_AUTOCREATE')),
                        'key'    => getenv('S3_ACCESS_KEY'),
                        'secret' => getenv('S3_SECRET_KEY'),
                        'hostname' => getenv('S3_HOST'),
                        'port' => intval(getenv('S3_PORT') ? getenv('S3_PORT') : 80),
                        'use_ssl' => boolval(getenv('S3_USE_SSL')),
                        'use_path_style'=> true
                    ]
                ]
            ];
        }
        else return [];
    })();
?>
