<?php
    $CONFIG = [
        'default_language' => 'de',
        'allow_user_to_change_display_name' => false,
        'auth.bruteforce.protection.enabled' => true,

        //Bin and Versioning
        'trashbin_retention_obligation' => '30, auto',
        'versions_retention_obligation' => '30, auto',
    ];
    
?>
