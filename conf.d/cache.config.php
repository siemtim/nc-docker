<?php
    $CONFIG = (function(){
        $host = getenv('REDIS_HOST');
        $c = [
            'memcache.local' => '\OC\Memcache\APCu'
        ];
        if ($host) {
            $c['memcache.distributed'] = '\OC\Memcache\Redis';
            $c['memcache.locking']     = '\OC\Memcache\Redis';
            $redis                     = [
                'host' => $host,
                'timeout'  => 1.5
            ];

            if (getenv('REDIS_PORT')) $redis['port'] = intval(getenv('REDIS_PORT'));
            if (getenv('REDIS_PASSWORD')) $redis['password'] = getenv('REDIS_PASSWORD');
            if (getenv('REDIS_DB_INDEX')) $redis['dbindex'] = intval(getenv('REDIS_DB_INDEX'));

            $c['redis'] = $redis;
        }
        return $c;
    })();
?>
