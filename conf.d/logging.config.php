<?php
    $CONFIG = [
        "log_type" => "errorlog",
        "loglevel" => (function(){
            switch(getenv("NEXTCLOUD_LOG_LEVEL")) {
                case "DEBUG": return 0;
                case "INFO" : return 1;
                case "WARN" : return 2;
                default     : return 3;
            }
        })(),
        "logdateformat" => "F d, Y H:i:s",
    ];

?>
