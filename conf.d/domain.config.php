<?php
    $CONFIG = (function(){
        if (getenv('NEXTCLOUD_TRUSTED_DOMAINS')) {
            return [
                'trusted_domains' => explode(',', getenv('NEXTCLOUD_TRUSTED_DOMAINS'))
            ];
        }
        else return [];
    })();
?>
