FROM ubuntu:20.04 AS download

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app

COPY nextcloud_url.txt app_urls.txt scripts/download.sh ./
COPY tmp/ /download/

RUN apt-get update && \
    apt-get -y install \
        curl \
        sed \
        openssl \
        xxd \
        && \
    mkdir apps && \
    chmod +x download.sh && \
    ./download.sh && \
    rm download.sh nextcloud_url.txt app_urls.txt

#--------------------------------------------------------------------------------------------------------------------------------------------------

FROM ubuntu:20.04 AS ncbuild

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app

COPY --from=download /app /download
COPY remove_apps.txt scripts/build.sh ./

RUN apt-get update && \
    apt-get -y install \
        curl \
        sed \
        tar \
        && \
    tar -xf /download/nextcloud.tar.bz2 && \
    rm -Rf nextcloud/data && \
    rm -Rf nextcloud/config && \
    mkdir nextcloud/config && \
    chmod +x ./build.sh && \
    ./build.sh && \
    cp /download/ENABLE_APPS ./nextcloud/.ENABLE_APPS && \
    chmod -R 755 nextcloud/
 
#--------------------------------------------------------------------------------------------------------------------------------------------------

FROM golang:1.17-buster AS gobuild

COPY bootstrap /app
WORKDIR /app

RUN go build .

#--------------------------------------------------------------------------------------------------------------------------------------------------

FROM php:7.4-apache-buster

ENV DEBIAN_FRONTEND=noninteractive

#Copy the install Script
COPY scripts/install.sh /usr/local/bin/

#Copy bootstrapper
COPY --from=gobuild /app/nextcloud-bootstrap /usr/local/bin/bootstrap

#Copy Aditional PHP Configuration
COPY php.ini.d/* $PHP_INI_DIR/conf.d/

#Copy Apace Site Configuration
COPY nc-site.conf /etc/apache2/sites-available/nextcloud.conf

#Copy conf.d data
COPY conf.d /conf.d/

RUN rm -R /var/www/html

#Copy Nextcloud Files
COPY --from=ncbuild --chown=root:www-data /app/nextcloud /var/www/html

WORKDIR /var/www/html/

RUN whoami && \
    chmod 555 /usr/local/bin/install.sh && \
    chmod 555 /usr/local/bin/bootstrap && \
    echo "Running installer" && \
    bash /usr/local/bin/install.sh && \
    rm -f /usr/local/bin/install.sh

VOLUME /var/nc-data/
VOLUME /var/www/html/config/

ENTRYPOINT [ "docker-php-entrypoint" ]
CMD [ "bootstrap", "apache2-foreground"]

