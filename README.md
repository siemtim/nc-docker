# About The Project

[Nextcloud](https://github.com/nextcloud) is a Software for collaboration. It can be used to store and share files and the functionality can be expanded by plugins, called *apps*. Some are maintained in the Nextcloud GitHub account, others are maintained by the community.

Nextcloud is built for easy installation, configuration and mainenance, especially for small environemts like an installation in a hosted webspace. Installation, updates and App management can be  done from the web UI. The configuration of the Nextcloud is stored in a php file (config.php) and can be updated from the web UI. Some important configuration options are set by the installation or update process.

This makes it easy to run a small Nextcloud in a single host setup. But it is very difficult and error prone to run Nextcloud in an multi instance setup.

The Nextcloud Container image tries to keep all the advantages of a Nextcloud installed on an web space, but this is not very handy when running Nextcloud using Containers. This Repository tries to deliver a better Nextcloud image with better security and automation. The Repository does not try to keep all the advantages of Nextcloud and disables features which are not helpful in an containerized environment. 

The following disadvantages of Nextcloud cannot be optimized:
- Nextcloud needs sticky sessions. Running Nextcloud without sticky session causes errors.
- Nextcloud is based on php and depends on php extensions which are difficult to install. This makes the contianer image very big.

# Structure

The Repository has the following structure:
- **bootstrap**: A small application written in go to automate the manual administration needs of nextcloud
- **conf.d**: This are configuration files which are copied to the nextcloud config folger. This files set configuration properties (static) ore extract them from the environment.
- **nc-php:** For Build Performance the configuration of php and the installation of php extensions are externalized. This is WIP.
- **php.ini.d**: php.ini files which are included in the php image
- **scripts**: Scripts used while image building
- **test**: Files used for testing
- **tmp**: Cache directory for downloading nextcloud files to improve build speed

