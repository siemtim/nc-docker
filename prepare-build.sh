#!/bin/bash

NC_URL=$(cat ./nextcloud_url.txt | sed 's/ *$//g')
NC_URL_HASH=$(echo ${NC_URL} | openssl dgst -md5 -binary | xxd -p)

if ! [[ -f "./tmp/${NC_URL_HASH}" ]]
then
    curl -fSLo "./tmp/${NC_URL_HASH}" "${NC_URL}"
fi

cat ./app_urls.txt | grep -v '^$' | while read LINE
do
    URL=$(echo "$LINE" | awk '{print $1}')

    URL_HASH=$(echo ${URL} | openssl dgst -md5 -binary | xxd -p)

    if ! [[ -f "./tmp/${URL_HASH}" ]]
    then
        curl -fqSLo "./tmp/${URL_HASH}" "${URL}"
    fi
done
